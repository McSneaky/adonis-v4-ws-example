
const ws = adonis.Ws().connect()

ws.on('open', () => {
  const chat = ws.subscribe('chat')
  chat.on('message', (message) => {
    let elem = document.createElement('div');
    elem.innerText = message;
    document.getElementById('adonis').appendChild(elem)
  })

  chat.on('ready', () => {
    chat.emit('message', 'Initial message')
  })
})
