'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class WsMiddleware {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async wsHandle({ request }, next) {
    let rand = Math.random()
    console.log('Middleware runs once when it\'s initialized');

    if (rand < 0.5) {
      console.log('WS middleware failed randomly');
      throw new Error('Randomly failed')
    }

    // call next to advance the request
    await next()
  }
}

module.exports = WsMiddleware
