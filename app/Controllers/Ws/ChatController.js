'use strict'

class ChatController {
  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request

    // Start sending some random data
    let timeout = setInterval(() => {
      try {
        this.socket.broadcastToAll('message', Math.random())
      } catch (e) {
        // Clear timeout on errors
        clearTimeout(timeout)
      }
    }, 1000);
  }

  onMessage(message) {
    // Echo back messages
    this.socket.broadcastToAll('message', message)
  }
}

module.exports = ChatController
