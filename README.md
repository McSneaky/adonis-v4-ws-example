# AdonisJS v4 WebSocket example

Made just as example to share on [AdonisJS forums](https://forum.adonisjs.com/)

## Things to look for

- server.js
  - Inits WS server
- start/wsKernel.js
  - Registers WS middleware
- start/socket.js
  - WS routes
- app/Middleware/WsMiddleware.js
  - WS middleware itself, makes WS fail randomly
- app/Controllers/Ws/ChatController.js
  - Backend WS controller, that sends random data in a loop
- public/script.js
  - Frontend part of WS, sends initial message and displays random data
